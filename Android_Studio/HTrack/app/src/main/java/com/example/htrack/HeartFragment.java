package com.example.htrack;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import Bio.Library.namespace.BioLib;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.LogRecord;

public class HeartFragment extends Fragment {
    //UI

    TextView text;
    TextView text2;
    TextView text3;
    Button button;
    int i=0;


    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        private volatile int hr = -1, bat = -1, rr = -1;
        private volatile String x, y, z;
        private volatile Float temp = new Float(-1.0);
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case BioLib.MESSAGE_BLUETOOTH_NOT_SUPPORTED:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            button.setEnabled(false);
                            button.setVisibility(View.INVISIBLE);
                            text.setText("Bluetooth not supported");
                        }
                    });
                    break;

                case BioLib.MESSAGE_BLUETOOTH_NOT_ENABLED:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            button.setEnabled(false);
                            button.setVisibility(View.INVISIBLE);
                            text.setText("Bluetooth not Enable");
                        }
                    });
                    break;

                case BioLib.STATE_CONNECTING:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text.setText("Connecting...");
                        }
                    });
                    break;
                case BioLib.STATE_CONNECTED:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            button.setEnabled(false);
                            button.setVisibility(View.INVISIBLE);
                            text.setEnabled(false);
                            text.setVisibility(View.INVISIBLE);
                            text2.setVisibility(View.VISIBLE);
                            text3.setVisibility(View.VISIBLE);
                        }
                    });
                    break;
                case BioLib.UNABLE_TO_CONNECT_DEVICE:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text.setText("Unable to connect");
                        }
                    });
                    break;

                case BioLib.MESSAGE_RADIO_EVENT:
                    String str = "";
                    try {
                        str = new String((byte[]) msg.obj, "UTF8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                case BioLib.MESSAGE_DATA_UPDATED:
                    BioLib.Output out = (BioLib.Output) msg.obj;
                    if (hr != out.pulse) {
                        hr = out.pulse;
                    }
                    if (out.battery != bat) {
                        bat = out.battery;
                    }
                    if(getActivity()!=null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                text2.setText(hr + "\nbpm");
                                text3.setText(bat + "%");
                            }
                        });
                    }
                    HttpURLConnection connection=null;
                    boolean f =false;
                    i=i+1;
                    if(i==450) {
                        i=0;
                        try {
                            String charset = "UTF-8";
                            String query = String.format("heart=%s",
                                    URLEncoder.encode(hr + "", charset));
                            URL url = new URL("http://192.168.0.37:8888/heart" + "?" + query);
                            connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestProperty("Accept-Charset", charset);
                            connection.setRequestMethod("GET");

                            BufferedReader in = new BufferedReader(
                                    new InputStreamReader(connection.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();

                            while ((inputLine = in.readLine()) != null) {
                                response.append(inputLine);
                            }
                            in.close();
                            if (response.toString().equals("passou")) {
                                // Create the object of
                                // AlertDialog Builder class
                                AlertDialog.Builder builder
                                        = new AlertDialog
                                        .Builder(getContext());

                                // Set the message show for the Alert time
                                builder.setMessage("Your heart rate is above 200!!");
                                // Set Alert Title
                                builder.setTitle("Alert!");

                                // Set Cancelable false
                                // for when the user clicks on the outside
                                // the Dialog Box then it will remain show
                                builder.setCancelable(false);
                                // Set the positive button with yes name
                                // OnClickListener method is use of
                                // DialogInterface interface.

                                builder
                                        .setPositiveButton(
                                                "ok",
                                                new DialogInterface
                                                        .OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                        // When the user click yes button
                                                        // then app will close
                                                        dialog.dismiss();
                                                    }
                                                });
                                // Create the Alert dialog
                                AlertDialog alertDialog = builder.create();
                                // Show the Alert Dialog box
                                alertDialog.show();
                            }

                            connection.disconnect();

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (ProtocolException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                    break;
                case BioLib.MESSAGE_PEAK_DETECTION:
                    BioLib.QRS qrs = (BioLib.QRS) msg.obj;
                    if (rr != qrs.rr) {
                        rr = qrs.rr;
                    }
                    break;
                case BioLib.MESSAGE_ECG_STREAM:
                    try {
                        byte[][] ecg = (byte[][]) msg.obj;
                        float[] values = new float[ecg[0].length];
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                case BioLib.MESSAGE_ACC_UPDATED:
                    BioLib.DataACC dataACC = (BioLib.DataACC) msg.obj;
                    x = String.valueOf(dataACC.X);
                    y = String.valueOf(dataACC.Y);
                    z = String.valueOf(dataACC.Z);
                    break;
            }
        }
    };

    BioLib bio;
    private static String address = "00:23:FE:00:07:05";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_heart, container, false);
        text = v.findViewById(R.id.textbox);
        text2 = v.findViewById(R.id.bpm);
        text3 = v.findViewById(R.id.bateria);

        button = (Button) v.findViewById(R.id.connect);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                try {
                    bio = new BioLib(getContext(), mHandler);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(bio.Connect(address,5)){
                        System.out.println("deu");
                    }else{
                        System.out.println("nao deu");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                int i = 0;
                while (true) {
                    try {
                        if (!(!bio.SetRTC(new Date(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis())) && i < 3))
                            break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    i++;
                }

            }
        });

        return v ;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            bio.Disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
