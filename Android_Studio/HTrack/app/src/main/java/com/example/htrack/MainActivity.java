package com.example.htrack;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.htrack.ui.main.SectionsPagerAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Timer timer = new Timer();
    static public final int REQUEST_LOCATION = 1;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
            bottomNav.setOnNavigationItemSelectedListener(navListener);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HeartFragment()).commit();
        startTimer();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFrag = null;

            switch (menuItem.getItemId()){
                case R.id.nav_heart:
                    selectedFrag = new HeartFragment();
                    break;
                case R.id.nav_location:
                    selectedFrag = new LocationFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFrag).commit();

            return true;
        }
    };

    TimerTask tt;
    public void startTimer(){
        tt=new TimerTask() {
            @Override
            public void run() {
                try {
                    getDeviceLocation();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        timer.schedule(tt, 0, 50000);
    }
    private void getDeviceLocation() throws IOException {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        //Get premmissions
        if(ContextCompat.checkSelfPermission(this,FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if(ContextCompat.checkSelfPermission(this, COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                final Task location = mFusedLocationProviderClient.getLastLocation(); // get Last location from client
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Location currentLocation = (Location) task.getResult();
                            HttpURLConnection connection=null;

                            try {

                                String charset = "UTF-8";
                                String query = String.format("latitude=%s&longitude=%s",
                                        URLEncoder.encode(currentLocation.getLatitude()+"", charset),
                                        URLEncoder.encode(currentLocation.getLongitude()+"", charset));
                                URL url = new URL("http://192.168.0.37:8888/location" + "?" + query);
                                connection = (HttpURLConnection) url.openConnection();
                                connection.setRequestProperty("Accept-Charset", charset);
                                connection.setRequestMethod("GET");

                                BufferedReader in = new BufferedReader(
                                        new InputStreamReader(connection.getInputStream()));
                                String inputLine;
                                StringBuffer response = new StringBuffer();

                                while ((inputLine = in.readLine()) != null) {
                                    response.append(inputLine);
                                }
                                in.close();


                                if(response.toString().equals("estas_dentro")){
                                    // Create the object of
                                    // AlertDialog Builder class
                                    AlertDialog.Builder builder
                                            = new AlertDialog
                                            .Builder(MainActivity.this);

                                    // Set the message show for the Alert time
                                    builder.setMessage("You are inside a forbidden Zone!!");
                                    // Set Alert Title
                                    builder.setTitle("Alert!");

                                    // Set Cancelable false
                                    // for when the user clicks on the outside
                                    // the Dialog Box then it will remain show
                                    builder.setCancelable(false);
                                    // Set the positive button with yes name
                                    // OnClickListener method is use of
                                    // DialogInterface interface.

                                    builder
                                            .setPositiveButton(
                                                    "ok",
                                                    new DialogInterface
                                                            .OnClickListener() {

                                                        @Override
                                                        public void onClick(DialogInterface dialog,
                                                                            int which)
                                                        {

                                                            // When the user click yes button
                                                            // then app will close
                                                            dialog.dismiss();
                                                        }
                                                    });
                                    // Create the Alert dialog
                                    AlertDialog alertDialog = builder.create();
                                    // Show the Alert Dialog box
                                    alertDialog.show();
                                }

                                connection.disconnect();

                            }catch (MalformedURLException e){
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }finally {
                                if(connection!=null){
                                    connection.disconnect();
                                }
                            }
                        }
                    }
                });
            }
        }
    }

}