# Welcome to Htrack
This wiki contains all development information about the Htrack project developed for the Software Engineering class.


##What is Htrack?
Htrack was creat to help people to mantain track of their own cardiac beat and to see if he is in a restricted zone or not.
Using a sensor connected to a smartphone to record the user heart rate and geo location. The system will process the data and alert the smartphone user when his vitals are irregular or his location is in a restricted zone.

##Where we can use this ?
* This can be used in simulations of armed combat ;
* Can be use as a monitoring system to people in house-arrest , with electronic bracelet;
* Athlete training, ultra-marathons;
* VR experience enhancement;

##Features 
* Alert the user of unusual activity, such as irregular heart rate or prohibited zones.
* Access the processed data exposed in the rest API.


##Architecture
![Screenshot](img/arquitecture.png)


* Vitaljaket. 
* RestApi.
* MobileApp.
* Kafka.
* ELK components.
* Spring Boot.


## Manual 





#Vital Jacket

Vital Jacket is an ECG system provided by biodivices.

Features:

* Provide all stream control parameters (Sampling Frequency, Gain, etc.), 
* Mode Switching (Configuration/ Recording); 
* RTC settings; 
* Pushbutton event (on Bluetooth stream and SD Card)
* The sensor is connected to the user SmartPhone via bluetooth.

# GPS Location
The user GPS location is obtained from the existing GPS from the user SmartPhone.

# Android App
The Android app only has two tabs. The first one lets the user connect to the sensor via bluetooth after it has been paired. A new screen will then appear showing the user bpm and the battery percentage of the sensor.
In the second tab will be shown a world map centered in the user location and all the forbidden zones marked has red.
The app will also emit two alerts, one if the user has a dangerous bpm and the second one if the user in inside a forbidden zone. 
For all this to work the user as to give permission to access is bluetooth, internet connection and location.

![Screenshot](img/App1.png) ![Screenshot](img/App2.png)



#Kafka
Kafka was the main transporter of information of this project, we created two topics, one for gps coordinates and other to the heart rate.
We take the information that was posted in our rest api, and send it throw kafka to elk and the the processing needed.


#Deployment
![Screenshot](img/DEPLOYMENT.png) 

Maven plugin to use  docker-compose (<github>/dkanejs/docker-compose-maven-plugin );


#ELK
ELK was used to consume the data that comes from kafka, process it and expose it. By using logstash we were able to consume from two different topics, the topic that corresponds to the heartbeat and the one that corresponds to the latitude and longitude. Processing was then made in the data consumed that corresponds the latitude and  longitude in order to turn it into a geolocation so that it can be exposed in a map on kibana. The data was then sent to elasticsearch in two different indexes, one for each topic. The data is then exposed in kibana as we can see in the images, allowing us to see in real time the heartbeat and the geolocation in the map of the different users.

![Screenshot](img/elk1.png) ![Screenshot](img/elk2.png)




