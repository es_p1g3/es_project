package com.exemple.KafkaController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
 
@Service
public class KafkaProducer {
  private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);
  
  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;
  
  @Value("${jsa.kafka.topic}")
  String kafkaTopic = "p1g3";
  @Value("${jsa.kafka.topicHeart}")
  String kafkaTopic2 = "p1g3_heart";

  
  public void send(String data) {
      log.info("sending data='{}'", data);
      System.out.println(kafkaTopic);
      
      kafkaTemplate.send(kafkaTopic, data);
  }
  public void sendHeartBeat(String data) {
      log.info("sending data='{}'", data);
      System.out.println(kafkaTopic2);
      kafkaTemplate.send(kafkaTopic2, data);
  }
}