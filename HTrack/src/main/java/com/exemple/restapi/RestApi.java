package com.exemple.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.exemple.KafkaController.KafkaProducer;
import com.exemple.KafkaController.MessageStorage;
import com.exemple.model.Location;
import com.exemple.model.Proibidas;

@Controller
@RestController
public class RestApi {
	
	static RestTemplate restTemplate = new RestTemplate();
	@Autowired
	KafkaProducer producer;
	
	@Autowired
	MessageStorage storage;
	private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);
	
	public String producers(String data){
	    producer.send(data);
	    
	    return "Done";
	}

	
	@RequestMapping(value="/location", method = RequestMethod.GET)
	public String getLocation(@RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude) {
		Location l = new Location();
		l.longitude = Float.parseFloat(longitude);
		l.latitude = Float.parseFloat(latitude);
		String datas = latitude + " " + longitude;
		producer.send(datas);
		System.out.println("xau");
		log.info("log data='{}'", latitude + longitude);

		System.out.println(longitude + "-" + latitude );
		
		//proibidas
		Proibidas pro = getProp();
		boolean result = false;
		int i;
		int j;
		double longitudeD = Double.parseDouble(longitude);
		double latitudeD = Double.parseDouble(latitude);
		double[] longitudes = {pro.x1,pro.x2,pro.x3,pro.x4};
		double[] latitudes =  {pro.y1,pro.y2,pro.y3,pro.y4};

		
		for (i = 0, j = longitudes.length - 1; i < longitudes.length; j = i++) {
	        if ((latitudes[i] > latitudeD) != (latitudes[j] > latitudeD) &&
	            (longitudeD < (longitudes[j] - longitudes[i]) * (latitudeD - latitudes[i]) 
	            		/ (latitudes[j]-latitudes[i]) + longitudes[i])) 
	        {
	          result = !result;
	         }
	      }
		if(result) return "estas_dentro";
		else return "estas_fora";

	}
	
	@GetMapping(value="/consumer")
	public String getAllRecievedMessage(){
		String messages = storage.toString();
	    storage.clear();
	    return messages;
	}


	
	@RequestMapping(value="/proibidas", method = RequestMethod.GET)
	public Proibidas getProp() {
		Proibidas pro = new Proibidas();
		pro.x1= 40.632514;
		pro.y1=-8.660307;
		pro.x2=40.633345;
		pro.y2=-8.660429;
		pro.x3=40.633388;
		pro.y3=-8.658966;
		pro.x4=40.632677;
		pro.y4=-8.658770;
		return pro;
	}
	@RequestMapping(value="/isInProibidas", method = RequestMethod.GET)
	public Boolean getisInProibidas(@RequestParam("longitude") String longitude, @RequestParam("latitude") String latitude) {
		//Example that is true longitude=40.633&latitude=-8.66
		Proibidas pro = getProp();
		boolean result = false;
		int i;
		int j;
		double longitudeD = Double.parseDouble(longitude);
		double latitudeD = Double.parseDouble(latitude);
		double[] longitudes = {pro.x1,pro.x2,pro.x3,pro.x4};
		double[] latitudes =  {pro.y1,pro.y2,pro.y3,pro.y4};

		
		for (i = 0, j = longitudes.length - 1; i < longitudes.length; j = i++) {
	        if ((latitudes[i] > latitudeD) != (latitudes[j] > latitudeD) &&
	            (longitudeD < (longitudes[j] - longitudes[i]) * (latitudeD - latitudes[i]) 
	            		/ (latitudes[j]-latitudes[i]) + longitudes[i])) 
	        {
	          result = !result;
	         }
	      }
		return result;
	}
	@RequestMapping(value="/heart", method = RequestMethod.GET)
	public String getHeart(@RequestParam("heart") String heart) {
		producer.sendHeartBeat(heart);
		if(Integer.parseInt(heart) >= 200) return "passou";
		else return "nao_passou";
	}
	
	
}
