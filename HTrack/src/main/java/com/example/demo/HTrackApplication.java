package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.exemple.restapi","com.exemple.KafkaController"})
@EntityScan({"com.exemple.model"})
public class HTrackApplication {

	public static void main(String[] args) {
		SpringApplication.run(HTrackApplication.class, args);
		System.out.println("ola");
	}

}
